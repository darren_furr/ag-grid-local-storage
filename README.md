# AG Grid Local Storage Module

This module can be added to AG Grid to allow column state to be automatically persisted to local storage automatically and be re-hydrated on grid ready.

## React Example

```jsx
import { LocalStorageModule } from 'ag-grid-local-storage'

<AgGridReact
    ...
    context={{ id: 'Storage ID' }}
    modules={[LocalStorageModule]}
/>
```

### Options

#### `context`

It is _recommended_ to set an `id` in the context, as this is used to create the local storage key. If this is ommited then the `id` will default to `"root"` and may be ovewritten by other grids
