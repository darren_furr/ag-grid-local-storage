import { pipe } from "ramda";
import { parseJson } from "./logic";

export const persistItem = (key: string, value: any) =>
  localStorage?.setItem(key, JSON.stringify(value));

export const getItem = pipe(
  (key: string): any => localStorage?.getItem(key),
  parseJson
);

export const deleteItem = (key: string) => localStorage?.removeItem(key);
