import { tryCatch, identity } from "ramda";

const parseValue = (value: string) => JSON.parse(value);
export const parseJson = tryCatch(parseValue, identity);
