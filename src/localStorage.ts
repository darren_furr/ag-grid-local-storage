import { Module } from "@ag-grid-community/core";

import { LocalStorageController } from "./localStorage/LocalStorageController";

export const LocalStorageModule: Module = {
  moduleName: "LocalStorageModule",
  beans: [LocalStorageController],
};
