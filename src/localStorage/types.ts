export enum LocalStorageKeys {
  COLUMN_STATE = "ag-grid_column-state",
}

export interface StorageController {
  destroy(): void;
}
