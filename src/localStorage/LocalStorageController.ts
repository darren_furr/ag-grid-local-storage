import {
  Autowired,
  BeanStub,
  Events,
  GridApi,
  PostConstruct,
  ColumnApi,
  PreDestroy,
  GridOptionsWrapper,
} from "@ag-grid-community/core";

import { getItem, persistItem } from "../utils";
import { LocalStorageKeys, StorageController } from "./types";

export class LocalStorageController
  extends BeanStub
  implements StorageController {
  // @ts-ignore
  @Autowired("gridApi") private gridApi: GridApi;
  // @ts-ignore
  @Autowired("columnApi") private columnApi: ColumnApi;
  @Autowired("gridOptionsWrapper")
  // @ts-ignore
  private gridOptionsWrapper: GridOptionsWrapper;

  // Events that will cause the column state to persist to local storage
  private columnStateEvents = [
    Events.EVENT_COLUMN_VISIBLE,
    Events.EVENT_COLUMN_PINNED,
    Events.EVENT_COLUMN_RESIZED,
    Events.EVENT_COLUMN_MOVED,
    Events.EVENT_FILTER_CHANGED,
    Events.EVENT_SORT_CHANGED,
  ];

  @PostConstruct
  init(): void {
    // Map the column state events to the persister
    this.columnStateEvents.map((event) => {
      this.addManagedListener(
        this.eventService,
        event,
        this.persistColumnState
      );
    });

    this.addManagedListener(this.eventService, Events.EVENT_GRID_READY, () => {
      // Get any existing state and set it to the grid
      const existingState = getItem(this.getColumnStateKey());

      if (existingState) {
        this.columnApi.setColumnState(existingState);
      }
    });
  }

  @PreDestroy
  destroy(): void {
    // Before destroying the grid, do one last persist
    this.persistColumnState();
  }

  private getUniqueGridKey = () =>
    // @ts-ignore
    this.gridOptionsWrapper.gridOptions.context?.id ?? "root";

  private getColumnStateKey = () => {
    const uniqueKey = this.getUniqueGridKey();

    return `${LocalStorageKeys.COLUMN_STATE}:${uniqueKey}`;
  };

  private persistColumnState = (): void => {
    const state = this.columnApi.getColumnState();
    persistItem(this.getColumnStateKey(), state);
  };
}
